package com.fvaldeon.mongoeventos.mvc;

import com.fvaldeon.mongoeventos.base.Artista;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.HashSet;
import java.util.Iterator;

/**
 * Created by profesor on 15/02/2019.
 */
public class Modelo {

    private static final String BBDD = "conciertos";
    private static final String COLECCION_ARTISTAS = "artistas";

    private MongoClient cliente;
    private MongoDatabase bbdd;
    private MongoCollection colArtistas;


    public void conectar() {
        cliente = new MongoClient();
        bbdd = cliente.getDatabase(BBDD);
        colArtistas = bbdd.getCollection(COLECCION_ARTISTAS);
    }

    public void desconectar() {
        cliente.close();
    }

    public Document artistaToDocument(Artista artista) {
        Document doc = new Document();
        doc.append("nombre", artista.getNombre())
                .append("estilo", artista.getEstilo())
                .append("cache", artista.getCache());
        return doc;
    }

    public Artista docucumentToArtista(Document doc) {
        Artista artista = new Artista();
        artista.setId(doc.getObjectId("_id"));
        artista.setNombre(doc.getString("nombre"));
        artista.setEstilo(doc.getString("estilo"));
        artista.setCache(doc.getDouble("cache"));

        return artista;
    }

    public void insertarArtista(Artista artista) {
        colArtistas.insertOne(artistaToDocument(artista));
    }

    public void eliminarArtista(Artista artista) {
        colArtistas.deleteOne(artistaToDocument(artista));
    }

    public void modificarArtista(Artista artista){
        colArtistas.replaceOne(new Document("_id", artista.getId()), artistaToDocument(artista));
    }


    public HashSet<Artista> getArtistas(){
        HashSet<Artista> set = new HashSet<>();

        Iterator<Document> it = colArtistas.find().iterator();
        while(it.hasNext()){

            set.add(docucumentToArtista(it.next()));
        }
        return set;
    }

    public Artista getArtista(String nombre){
        return docucumentToArtista((Document)colArtistas.find(new Document("nombre", nombre)).first());
    }
}