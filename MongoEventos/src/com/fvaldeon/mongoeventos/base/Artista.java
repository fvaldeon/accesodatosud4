package com.fvaldeon.mongoeventos.base;


import org.bson.types.ObjectId;

/**
 * Created by profesor on 15/02/2019.
 */
public class Artista {

    private ObjectId id;
    private String nombre;
    private String estilo;
    private Double cache;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEstilo() {
        return estilo;
    }

    public void setEstilo(String estilo) {
        this.estilo = estilo;
    }

    public Double getCache() {
        return cache;
    }

    public void setCache(Double cache) {
        this.cache = cache;
    }

    @Override
    public String toString() {
        return nombre + " " + estilo;
    }
}
