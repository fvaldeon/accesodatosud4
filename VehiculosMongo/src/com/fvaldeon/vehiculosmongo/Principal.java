package com.fvaldeon.vehiculosmongo;

import com.fvaldeon.vehiculosmongo.gui.Controlador;
import com.fvaldeon.vehiculosmongo.gui.Modelo;
import com.fvaldeon.vehiculosmongo.gui.Vista;


public class Principal {
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista, modelo);
    }
}
