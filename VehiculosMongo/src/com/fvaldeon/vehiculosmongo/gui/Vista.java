package com.fvaldeon.vehiculosmongo.gui;

import com.fvaldeon.vehiculosmongo.base.Coche;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;

/**
 * Created by Profesor on 15/02/2018.
 */
public class Vista {
    private JPanel panel1;
     JTextField txtMarca;
     JTextField txtMatricula;
     JTextField txtModelo;
     JTextField txtBuscar;
     JList<Coche> list1;
     JButton borrarBtn;
     JButton nuevoBtn;
     JButton modificarBtn;
     DatePicker datePicker;
     DefaultListModel<Coche> dlm;


    public Vista() {
        JFrame frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        inicializar();
    }

    private void inicializar(){
        dlm = new DefaultListModel<>();
        list1.setModel(dlm);

        datePicker.getComponentToggleCalendarButton().setText("Cal");
    }

}
